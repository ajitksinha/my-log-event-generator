package com.example.log;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.log.entity.Event;
import com.example.log.model.EventType;
import com.example.log.repo.EventRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class LogApplicationTests {

	 @Autowired
	    private EventRepository repository;

	    @Test
	    public void whenFindingCustomerById_thenCorrect() {
	        Event alert = new Event();
	        alert.setId("alert-1");
	        alert.setDuration(3);
	        alert.setHost("127.0.0.1");
	        alert.setType(EventType.APPLICATION_LOG);

	        repository.save(alert);
	        assertThat(repository.findById("alert-1")).isInstanceOf(Optional.class);
	    }

	    @Test
	    public void whenFindingAllCustomers_thenCorrect() {
	        Event event1 = new Event();
	        event1.setId("event1");
	        event1.setDuration(3);
	        event1.setHost("127.0.0.1");
	        event1.setType(EventType.APPLICATION_LOG);

	        Event event2 = new Event();
	        event2.setId("event2");
	        event2.setDuration(7);
	        event2.setHost(null);
	        event2.setType(null);
	        event2.setAlert(Boolean.TRUE);

	        repository.save(event1);
	        repository.save(event2);

	        assertThat(repository.findAll()).isInstanceOf(List.class);
	    }
}
