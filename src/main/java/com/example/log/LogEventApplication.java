package com.example.log;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.example.log.service.LogGeneratorService;

@SpringBootApplication
@ComponentScan(basePackages = "com.example.log.*")
public class LogEventApplication  {
  

    @Autowired
    private LogGeneratorService service ;

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(LogEventApplication.class);      
        app.run();
    }

   

	public void run() throws Exception {
		service.generateLogFile("src/main/resources/logs.txt", 5000);		
		service.parseAndPersistEvents();
		
	}
}