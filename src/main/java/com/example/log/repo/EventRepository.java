package com.example.log.repo;

import org.springframework.data.repository.CrudRepository;

import com.example.log.entity.Event;

public interface EventRepository extends CrudRepository<Event, String> {
}
