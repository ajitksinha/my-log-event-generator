package com.example.log.service;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.example.log.entity.Event;
import com.example.log.model.EventDto;
import com.example.log.model.State;
import com.example.log.repo.EventRepository;
import com.example.log.util.LogFileGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service 
public class LogGeneratorService {

	private static final Logger LOGGER = LoggerFactory.getLogger(LogGeneratorService.class);

	@Autowired
	private EventRepository eventRepository;
	
	@Autowired
	private LogFileGenerator logFileGenerator;
	
	
	@Value("${app.my-log-event-generator.alert-threshold-ms}")
	private String eventDuration;

	 public  void generateLogFile(String path, int numberOfEvents) {
		 logFileGenerator.generateFile(path, numberOfEvents);
	 }

	public void parseAndPersistEvents() {

        // EventMap temporarily holds the events while we find the matching-STARTED or FINISHED-events.
        // Once found, the corresponding event would be removed from the map.
        Map<String, EventDto> eventMap = new HashMap<>();

        //Alerts map holds the events that are parsed before persisting in a DB table.
        // Each alert would have its execution time calculated and flagged (isAlert TRUE or FALSE).
        Map<String, Event> alerts = new HashMap<>();

        LOGGER.info("Parsing the events and persisting the alerts. This may take a while...");
        try (LineIterator li = FileUtils.lineIterator(new ClassPathResource("samples/logfile.txt").getFile())) {
            
            while (li.hasNext()) {
                EventDto event;
                try {
                    event = new ObjectMapper().readValue(li.nextLine(), EventDto.class);
                    LOGGER.trace("{}", event);

                    // Check if we have either STARTED or FINISHED event already for the given ID.
                    // If yes, then find the execution time between STARTED and FINISHED states and update the alert.
                    if (eventMap.containsKey(event.getId())) {
                        EventDto e1 = eventMap.get(event.getId());
                        long executionTime = getEventExecutionTime(event, e1);

                        // the alert created off an event would have the alert flag set to FALSE by default.
                        Event alert = new Event(event, Math.toIntExact(executionTime));

                        // if the execution time is more than the specified threshold, flag the alert as TRUE
                        if (executionTime > Long.parseLong(eventDuration)) {
                            alert.setAlert(Boolean.TRUE);
                            LOGGER.trace("!!! Execution time for the event {} is {}ms", event.getId(), executionTime);
                        }

                        // add it to the pool of alerts that are yet to be persisted
                        alerts.put(event.getId(), alert);

                        // remove from the temporary map as we found the matching event
                        eventMap.remove(event.getId());
                    } else {
                        eventMap.put(event.getId(), event);
                    }
                } catch (JsonProcessingException e) {
                    LOGGER.error("Unable to parse the event! {}", e.getMessage());
                }

              
            } // END while
            if (alerts.size() != 0) {
                persistAlerts(alerts.values());
            }
        } catch (IOException e) {
            LOGGER.error("!!! Unable to access the file: {}", e.getMessage());
        }
    		
		
		
	}
	
	  private void persistAlerts(Collection<Event> alerts) {
	        LOGGER.debug("Persisting {} alerts...", alerts.size());
	        eventRepository.saveAll(alerts);
	    }

	    private long getEventExecutionTime(EventDto event1, EventDto event2) {
	        EventDto endEvent = Stream.of(event1, event2).filter(e -> State.FINISHED.equals(e.getState())).findFirst().orElse(null);
	        EventDto startEvent = Stream.of(event1, event2).filter(e -> State.STARTED.equals(e.getState())).findFirst().orElse(null);

	        return Objects.requireNonNull(endEvent).getTimestamp() - Objects.requireNonNull(startEvent).getTimestamp();
	    }

}
