package com.example.log.util;



import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.example.log.model.EventDto;
import com.example.log.model.EventType;
import com.example.log.model.State;

@Component
public class LogFileGenerator {
    

    public void generateFile(String path, int numberOfEvents) {
        List<EventDto> events = new ArrayList<>();

        for (int i = 0; i < numberOfEvents * 2; i++) {
            String id = generateRandomUuid();
            State state = chooseFrom(State.STARTED, State.FINISHED);
            EventType type = chooseFrom(EventType.APPLICATION_LOG, null);
            long timestamp = System.currentTimeMillis();
            String host = chooseFrom(generateRandomIp(), null);
            EventDto startEvent = new EventDto();
            startEvent.setId(id);
            startEvent.setState(State.STARTED);
            startEvent.setHost(host);
            startEvent.setType(type);
            startEvent.setTimestamp(timestamp);

            EventDto endEvent = new EventDto();
            endEvent.setId(id);
            endEvent.setState(State.FINISHED);
            endEvent.setHost(host);
            endEvent.setType(type);
            endEvent.setTimestamp((long) (timestamp + Math.random() * System.nanoTime() % 10));

            events.add(startEvent);
            events.add(endEvent);
        }
        
        writeToFile(path, events);
    }

    private void writeToFile(String path, List<EventDto> events) {
      
        File file = null;
        FileOutputStream fileOutputStream = null;
        try {
            file = new File(path);
            file.createNewFile();
            fileOutputStream = new FileOutputStream(file);
            for (EventDto e : events) {
                fileOutputStream.write(e.toString().getBytes(StandardCharsets.UTF_8));
                fileOutputStream.write("\n".getBytes(StandardCharsets.UTF_8));
            }
           

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private  <T> T chooseFrom(T... values) {
        return values[(int) (Math.random() * System.nanoTime() % values.length)];
    }

    private  String generateRandomIp() {
        return (int) (Math.random() * System.nanoTime() % 255) + "." +
                (int) (Math.random() * System.nanoTime() % 255) + "." +
                (int) (Math.random() * System.nanoTime() % 255) + "." +
                (int) (Math.random() * System.nanoTime() % 255);
    }

    public String generateRandomUuid() {
        String seed = "e0A1f2E3D4a5Bd6b7c8CF9";
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < 16; i++) {
            sb.append(seed.charAt((int) ((Math.random() * System.nanoTime()) % seed.length())));
        }

        return sb.toString();
    }

	}
